//login
enter name
enter room code

//lobby
lookup room
create or join room
set room status to waiting

//start
set room status to playing
pick first judge
pick first question

//judge question
show status of people

//people question
show status of people
enter answer

//judge answer
set choice of answer
see if everyone has voted
go to choice screen if yes or give 10 more seconds
stop voting

//voter answer
cast vote for answer
stop voting
calculate vote winner

//show answers
give player point
show choice answer

//show vote
give player point
show vote winner

//end
check if player has more than 10 points
end game or ask another question

var rooms = {UnCR9q1bgtiaY4rEAAAH: {code: 'XJLC', users: ['Jim', 'David']}, judge: 'David', status: 'waiting'};
var people = {XPdrEd9tR272l7ymAAAG: {name: 'Jim', winner: 'yes', role: 'judge', status: 'typing', voted: 'yes', points: 1}};
var questions = {1: {question: 'What really killed the dinosuars?', choice: 1, votes: {1: 0, 2: 0, 3: 0, 4: 1}}};
var answers = {1: {1: 'Smoking', 2: 'AIDS', 3: 'Mammals', 4: 'Bananas'}};

{ rooms:
  { WXLC:
    { users:
      { xldfasdflkj: {name: 'Jim', blah: 'blah'},
        xfasdfkjavo: {name: 'Bill', blah: 'blah'}
      }
    },
    { questions:
      { 1:
        { question: 'What is that?',
          answers: {xldfasdflkj: ''}
        }
      }
    }
  }
}