// Modules
import Vue from 'vue'
// import VueRouter from 'vue-router'
import VueSocketio from 'vue-socket.io'
import _ from 'lodash'

// Install
// Vue.use(VueRouter)
Vue.use(VueSocketio, 'ws://localhost:3000')
Vue._ = _

// Components
import App from './App'
// import game from './components/game'
import lobby from './components/lobby'
Vue.component('lobby', lobby)

// Routing
// const routes = [
//   { path: '/game', component: game },
//   { path: '/lobby', component: lobby }
// ]

// const router = new VueRouter({
//   routes
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
  // router: router
})
