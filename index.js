var express = require('express');
var compress = require('compression');
var serveStatic = require('serve-static');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash');
var Room = require('./rooms.js');
var things = require('./things.js');



//Express
app.use(compress());
// app.use(serveStatic('public', {
//   'index': false,
//   'setHeaders': setHeaders
// }));
app.use(serveStatic('vue/dist/', {
  'index': false,
  'setHeaders': setHeaders
}));

function setHeaders(res, path) {
  res.setHeader('Cache-Control', 'public, max-age=31536000');
}

app.get('/', function(req, res){
  console.log(req.ip);
  res.sendFile('index.html', {root: __dirname});
});

app.get('/alpha', function(req, res){
  console.log(req.ip);
  res.sendFile('alpha.html', {root: __dirname});
});

app.get('/beta', function(req, res){
  console.log(req.ip);
  res.sendFile('vue/dist/index.html', {root: __dirname});
});

//Socket listen
http.listen((process.env.PORT || 3000), function(){
  console.log('[ LISTEN       ]    Listening on port 3000.');
});



//Logic
var rooms = {};
var nameText = ['David', 'Sarah', 'Jake', 'Alex', 'Tyrone'];
var judgeText = ['will be judging all of you! Well, more than usual...', 'will judge the next round.', 'is the judge this turn.', 'will decide your fate! Or just pick the answer.', 'is the judge.'];
var promptText = things;

//Events
io.on('connection', function(socket){
  console.log('[ CONNECT      ]    A user connected on socket ' + socket.id + '.');

  //Create TEMP room
  var debug = true;
  var key =  'TEMP';
  rooms[key] = new Room({id: key});

  var current = rooms[key]['current'],
      currentQuestion = rooms[key]['current']['question'],
      currentJudge = rooms[key]['current']['judge'],
      currentWinner = rooms[key]['current']['winner'];

  //Inititalize
  socket.emit('jazz', {})
  socket.emit('init', {
    clientID: socket.id,
    name: nameText[_.random(nameText.length - 1)],
    roomKey: key
  });

  var heartbeat = function() {
    io.to(key).emit('status', {
      users: rooms[key].users,
      questions: rooms[key].questions,
      current: rooms[key].current
    });

    console.log('[' + key + '][ STATUS ]');
  };

  //Lobby
  socket.on('lobby', function(data, fn) {
    console.log('[ LOBBY        ]    Send list of games.');
    io.emit('lobby', rooms);
  });

  //Join
  socket.on('join', function(data, fn) {
    console.log(data)
    key = data.roomKey;
    data.id = socket.id;

    //Create room if it doesn't exist and add user
    if (!rooms[key]) {
      rooms[key] = new Room({key: key, judge: socket.id});
      rooms[key]['users'][socket.id] = new User(data);
      rooms[key]['questions'].push(new Question({judge: socket.id, judgeText: judgeText[_.random(judgeText.length - 1)], questionText: promptText[_.random(promptText.length - 1)]}));
      console.log('[' + key + '][ CREATE ]    The room was created.');
      console.log('[' + key + '][ JOIN   ]    The user ' + socket.id + ' joined.');
      console.log('[' + key + '][ JUDGE  ]    The user ' + socket.id + ' is the judge.');
    } else {
      rooms[key]['users'][socket.id] = new User(data);
      console.log('[' + key + '][ JOIN   ]    The user ' + socket.id + ' joined.');
    }

    console.log(rooms[key]);

    current = rooms[key]['current'];
    currentQuestion = rooms[key]['current']['question'];
    currentJudge = rooms[key]['current']['judge'];
    currentWinner = rooms[key]['current']['winner'];

    //Join Socket room
    socket.join(key);
    console.log(key)


    //Waiting
    io.to(key).emit('waiting', {
      users: rooms[key]['users'],
      currentQuestion: currentQuestion.id,
      judge: currentJudge['id'],
      judgeText: rooms[key]['questions'][currentQuestion.id]['judge']['text'],
      promptText: rooms[key]['questions'][currentQuestion.id]['text'],
      chat: rooms[key]['chat']
    });

    console.log(key)

    io.to(key).emit('currentScreen', {currentScreen: rooms[key]['current']['screen']['id']});

    console.log(rooms, key)

    //Start
    socket.on('start', function(data, fn) {
      console.log('[' + key + '][ START  ]    The user ' + socket.id + ' started the game in the room ' + key + '.');

      io.to(key).emit('judge', {
        judge: currentJudge['id'],
        judgeText: judgeText[_.random(4)],
        promptText: promptText[_.random(promptText.length - 1)]
      });

      rooms[key]['current']['screen']['id'] = 3;
      io.to(key).emit('currentScreen', {currentScreen: 2, circleColor: 'bg-cyan'});

      setTimeout(function() { io.to(key).emit('currentScreen', {currentScreen: 3}); }, 2000);
    });

    socket.on('startRound', function(data, fn) {
      io.to(key).emit('currentScreen', {currentScreen: 4});
    });

    socket.on('type', function(data, fn) {
      console.log('Typing...');

      //Set status to typing
      rooms[key]['users'][socket.id]['status'] = 1;
      
      heartbeat();
    });

    socket.on('submit', function(data, fn) {
      console.log('Answer submitted.');

      //Set status to submitted
      rooms[key]['users'][socket.id]['status'] = 2;
      
      //Set answer
      console.log('currentQuestion for answer submit ' + currentQuestion);

      var tempAnswer = new Answer({id: socket.id, text: data.answer});
      rooms[key]['questions'][currentQuestion.id]['answers'][socket.id] = tempAnswer;

      console.log(rooms[key]);

      //Everyone has answered
      if(_.size(rooms[key]['questions'][currentQuestion.id]['answers']) >= (_.size(rooms[key]['users']) - 1)) {
        io.to(key).emit('currentScreen', {currentScreen: 5});
      }

      io.to(key).emit('status', {
        users: rooms[key].users
      });

      io.to(key).emit('answers', {
        answers: rooms[key]['questions'][currentQuestion.id]['answers']
      });

      if (!rooms[key]['current']['winner']) {
        rooms[key]['current']['winner'] = [];
      }
    });

    //Choose answer
    socket.on('choose', function(data, fn) {
      console.log('choose ' + data.user);

      rooms[key]['current']['winner']['id'] = data.user;

      console.log(rooms[key]['current']['winner']);

      io.to(key).emit('winner', {
        winner: rooms[key]['current']['winner']['id']
      });

      console.log(rooms[key]['questions'][currentQuestion.id]['answers']);
    });

    socket.on('chosen', function(data, fn) {
      console.log('chosen');

      currentWinner = rooms[key]['current']['winner'];
      console.log(currentWinner.id);

      io.to(key).emit('winning-answer', {
        winningAnswer: rooms[key]['questions'][currentQuestion.id]['answers'][currentWinner.id]['text']
      });

      rooms[key]['users'][currentWinner.id]['score'] += 1;

      io.to(key).emit('status', {
        users: rooms[key].users,
        currentQuestion: currentQuestion
      });

      io.to(key).emit('currentScreen', {currentScreen: 6, circleColor: 'bg-magenta'});
    });

    socket.on('next-question', function() {

      console.log('asdfa;sldfkajsdf;laksdjf' + currentWinner.id);
      console.log(rooms[key]['users']);
      if (rooms[key]['users'][currentWinner.id]['score'] >= 5) {
        io.to(key).emit('currentScreen', {currentScreen: 7});
      } else {
        //Store winners

        //Update for next question
        console.log('currentQuestion before add ' + currentQuestion.id);
        currentQuestion['id'] += 1;
        console.log('currentQuestion after add ' + currentQuestion.id);

        rooms[key]['questions'].push(new Question({judge: socket.id, judgeText: judgeText[_.random(judgeText.length - 1)], questionText: promptText[_.random(promptText.length - 1)]}));

        io.to(key).emit('judge', {
          judge: rooms[key]['current']['winner']['id'],
          judgeText: judgeText[_.random(4)],
          promptText: promptText[_.random(promptText.length - 1)],
        });

        _.forEach(rooms[key]['users'], function(value, k) {
          rooms[key]['users'][k]['status'] = 0;
          console.log('blah');
        });

        io.to(key).emit('status', {
          users: rooms[key].users
        });

        io.to(key).emit('reset', {});

        console.log(rooms[key]);

        io.to(key).emit('currentScreen', {currentScreen: 3});
      }
    });

    socket.on('chat-send', function(data) {
      console.log(data);

      rooms[key]['chat'].push({name: data.name, text: data.chatText});

      io.to(key).emit('chat', {
        chat: rooms[key]['chat']
      });

      console.log(rooms[key]['chat']);
    });
  });



  //Disconnect and remove user
  socket.on('disconnect', function() {
    console.log('[ DISCONNECT   ]    The user ' + socket.id + ' left the room ' + key + '.');

    //Remove user from room
    if (rooms && rooms[key] && rooms[key]['users'] && rooms[key]['users'][socket.id]) {
      console.log('[' + key + '][ DELETE ]    User ' + socket.id + ' has been deleted. User left.');
      delete rooms[key]['users'][socket.id];
      heartbeat();
    }

    //Reassign judge if user that left was judge
    if (rooms[key]['current']['judge']['id'] === socket.id) {
      if (!_.isEmpty(rooms[key]['users'])) {
        rooms[key]['current']['judge']['id'] = _.result(_.find(rooms[key]['users'], 'id'), 'id');

        io.to(key).emit('judge', {
          judge: rooms[key]['current']['judge']['id'],
          judgeText: rooms[key]['questions'][currentQuestion.id]['judge']['text'],
          promptText: rooms[key]['questions'][currentQuestion.id]['text']
        });

        console.log('[' + key + '][ JUDGE  ]    The user ' + rooms[key]['current']['judge']['id'] + ' is the judge.');
      }
    }

    //Boot to waiting if less than 3 players
    if ((_.size(rooms[key]['users'])) < 3) {
      io.to(key).emit('currentScreen', {currentScreen: 1});
      io.to(key).emit('notificaiton', {text: 'Sorry, but you need at least 3 to play.'});
    }

    //Remove room if no users
    if (_.isEmpty(rooms[key]['users']) && key !== 'TEMP') {
      console.log('[' + key + '][ DELETE ]    Room has been deleted. All users left.');
      delete rooms[key];
    }
  });
});

var Room = function Room(data) {
  var self = this;

  self.key       = data.key;
  self.users     = {};
  self.questions = [];
  self.chat      = [];
  self.current   = {
    screen: {id: data.screen || 1},
    judge: {id: data.judge || 0},
    question: {id: 0, countdown: false},
    winner: {id: 0},
    notification: {text: ''}
  };
};

var User = function User(data) {
  var self = this;

  self.id     = data.id;
  self.name   = data.name;
  self.winner = data.winner || false;
  self.status = data.status || null;
  self.score  = data.points || 0;
};

var Question = function Question(data) {
  var self = this;

  self.judge   = {id: data.judge || false, text: data.judgeText || ''};
  self.text    = data.questionText || '';
  self.winners = {judge: false, votes: false};
  self.answers = {};
};

var Answer = function Answer(data) {
  var self = this;

  self.id = data.id || '';
  self.text = data.text || '';
  self.votes = 0;
};

var barn = { key: 'TEST',
  users:
  {
    '9iEuSI1fU1MlbUVPAAAD':
    {
      id: '9iEuSI1fU1MlbUVPAAAD',
      name: 'Tyrone',
      winner: false,
      status: 0,
      score: 2
    },
    'UxR0rtXK4tLuLsFbAAAE':
    {
      id: 'UxR0rtXK4tLuLsFbAAAE',
      name: 'Sarah',
      winner: false,
      status: 0,
      score: 3
    }
  },
  questions:
  [
    {
      judge: { id: 'UxR0rtXK4tLuLsFbAAAE' },
      text: "Things you shouldn't do in a cemetery.",
      winners:
      {
        judge: 'UxR0rtXK4tLuLsFbAAAE',
        votes: 'UxR0rtXK4tLuLsFbAAAE',
      },
      answers:
      {
        'UxR0rtXK4tLuLsFbAAAE':
        {
          id: 'UxR0rtXK4tLuLsFbAAAE',
          text: 'Corpses',
          votes: 2
        },
        '9iEuSI1fU1MlbUVPAAAD':
        {
          id: '9iEuSI1fU1MlbUVPAAAD',
          text: 'Mining',
          votes: 0
        }
      }
    }
  ],
  current:
  {
    screen: { id: 3 },
    judge: { id: '9iEuSI1fU1MlbUVPAAAD' },
    question: { id: 5 },
  }
};