var Service = function() {
    var self = this;
    self.rooms = {};

    self.init = function(key) {
        self.rooms[key] = {
            users: [],
            questions: []
        };

        return self.rooms[key];
    };

    self.get = function(key) {
        return self.rooms[key] || false;
    };

    self.set = function(key, data) {
        self.rooms[key] = data;
        return self.rooms[key];
    };

    self.destroy = function(key) {
        delete self.rooms[key];
        return {};
    };

    self.generateKey = function() {
        var key = '';

        for (var i = 0; i < 4; i++) {
            key += String.fromCharCode(65 + Math.floor(Math.random() * 26));
            if (self.get(key)) {
                key = '';
                i = 0;
            }
        }

        return key;
    };
};

module.exports = new Service();